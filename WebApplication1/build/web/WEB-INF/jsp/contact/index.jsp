<%-- 
    Document   : index
    Created on : Oct 15, 2014, 9:10:36 PM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
    </head>
    <body>
        <div>
            <%--
            <%@include file='cadastro.jsp'%> 
            --%>
            <form>
                <input name="id" hidden="true" id="idInput" />
                <label>First Name:</label> <input id="firstnameInput" name="firstname" /> <br />
                <label>Last Name:</label> <input name="lastname" id="lastnameInput" /><br />
                <label>Email:</label> <input name="email" id="emailInput" /><br />
                <label>Phone:</label> <input name="phone" id="phoneInput" /><br />
                <br />
                <input type="submit" name="action" id="saveContact" value="save" />
                <input type="submit" name="action" id="cancelar" value="Cancel"/>
                <a id="test" href="#">Click here</a>
            </form>
        </div> 
        <div id="content" style="margin: 10px;">
            <%--
            <%@include file='list.jsp'%> 
            --%>
            <table id="contactTableResponse">
                <tr>
                    <th>Id</th>
                    <th>First</th>
                    <th>Last</th>
                    <th>Email</th>
                    <th>Phone</th>
                </tr>
                <c:forEach var="contato" items="${contactForm}">
                    <tr>
                        <td>${contato.id}</h1>
                        <td>${contato.firstname}</td>
                        <td>${contato.lastname}</td>
                        <td>${contato.email}</td>
                        <td>${contato.phone}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#saveContact').submit(function (e) {
                    alert('haha');
                    $.post('/contact/savescript', $(this).serialize(), function (contact) {
                        $('#contactTableResponse').last().append(
                            '<tr>' +
                                '<td>' + contact.id + '</td>' +
                                '<td>' + contact.firstname + '</td>' + 
                                '<td>' + contact.lastname + '</td>' + 
                                '<td>' + contact.email + '</td>' +
                                '<td>' + contact.phone + '</td>' +
                            '</tr>');
                    });
                    alert('haha');
                    clearInputs(); //Limpar formulário
                    e.preventDefault();
                });
            });
        
        function clearInputs()
        {
            $('input[id*="Input"]').each(function() //procura por todos os ID's que termina com Input
            {
               $(this).val(''); 
            });
        }
        </script>
    </body>
</html>
