<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:choose>
    <c:when test="${contact.id == null}"><h2>Add Contact</h2></c:when>
    <c:when test="${contact.id != null}"><h2>Edit Contact</h2></c:when>
</c:choose>
    <form:form  id="saveContact"> <!--modelAttribute="contact" action="/WebApplication1/contact/save.html" > -->
    <input name="id" id="idInput" hidden="true" />
    <label>First Name:</label> <input id="firstnameInput" name="firstname"  /> <br />
    <label>Last Name:</label> <input id="lastnameInput" name="lastname" /><br />
    <label>Email:</label> <input id="emailInput" name="email" ><br />
    <label>Phone:</label> <input id="phoneInput" name="phone"    /><br />
    <br />
    <input type="submit" name="action" id="submitForm" value="save" />
    <input type="submit" name="action" id="cancelar" value="Cancel"/>
</form:form>
    