<%-- 
    Document   : list
    Created on : Oct 15, 2014, 8:20:10 PM
    Author     : root
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<table id="contactTableResponse">
    <tr>
        <th>Id</th>
        <th>First</th>
        <th>Last</th>
        <th>Email</th>
        <th>Phone</th>
        <th></th>
        <th></th>
    </tr>
<c:forEach var="contato" items="${contactForm}">
    <tr>
        <td>${contato.id}</h1>
        <td>${contato.firstname}</td>
        <td>${contato.lastname}</td>
        <td>${contato.email}</td>
        <td>${contato.phone}</td>
        <td><a href="/WebApplication1/contact/delete?id=${contato.id}">Delete</a></td>
        
    </tr>
</c:forEach>
</table>