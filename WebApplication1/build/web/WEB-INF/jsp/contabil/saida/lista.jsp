<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Size:${pessoas.size()}</h1>
<table border="1px" cellpadding="0" cellspacing="0">
<thead>
    <tr>
        <th>idSaida</th><th>descricao</th><th>dataSaida</th><th>Valor</th>
    </tr>
</thead>
<tbody>
    <c:forEach var="sa" items="${saidas}">
        <tr>
            <td>${sa.idSaida}</td>
            <td>${sa.descricao}</td>
            <td>${sa.dataSaida}</td>
            <td>${sa.valor}</td>
            <td>
                <a href="/WebApplication1/saida/edit/${sa.idSaida}.json">Edit</a><br/>
                <a href="/WebApplication1/saida/delete/${sa.idSaida}.json">Delete</a><br/>
            </td>
        </tr>
    </c:forEach>
</tbody>
</table>