<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
       
        
        
        <%--<script src="../../../js/jquery.min.js"></script>--%>
        <script type="text/javascript">
            $(document).ready(function() {
              $('#newSaidaForm').submit(function(event) {
                  /*var idPessoa = ; */
                  var idSaida = 0;//parseInt($('#idSaida').val(), 10);
                  var descricao = $('#descricao').val();
                  var dataSaida = $('#datepicker').val();//$('#dataSaida').val();
                  var valor = parseFloat($('#valor').val());
                  var json = { "idSaida" : idSaida, "descricao" : descricao, "dataSaida": dataSaida, "valor": valor};
                  
                $.ajax({
                        url: $("#newSaidaForm").attr( "action"),
                        data: JSON.stringify(json),
                        type: "POST",

                        beforeSend: function(xhr) {
                                xhr.setRequestHeader("Accept", "application/json");
                                xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(smartphone) {
                                var $container = $("#tabela");
                                $container.load("/WebApplication1/saida/list");
                        }
                });
                event.preventDefault();
              });
            }); 
            
            $(document).ready(function() {
    	
		var deleteLink = $("a:contains('Delete')");
      
		$(deleteLink).click(function(event) {
    	  
                    $.ajax({
                    url: $(event.target).attr("href"),
                    type: "DELETE",

                    beforeSend: function(xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                    },

                    success: function(smartphone) {
                            var respContent = "";
                            var rowToDelete = $(event.target).closest("tr");
                            rowToDelete.remove();

                            respContent += "<span class='success'>Smartphone was deleted: [";
                            respContent += smartphone.producer + " : ";
                            respContent += smartphone.model + " : " ;
                            respContent += smartphone.price + "]</span>";

                            $("#sPhoneFromResponse").html(respContent);   		
                    }
            });

            event.preventDefault();
            });

            });   
            
            
            //DatePicker
            $(function() {
                $( "#datepicker" ).datepicker();
            });
            
        </script>
    </head>
    <body>
        <div id="cadastro">
            <form:form id="newSaidaForm" action="/WebApplication1/saida/novo.json" commandName="saida">
                <form:input path="idSaida" hidden="true" /> 
                <table>
                    <tbody>
                        <tr>
                            <td>Descrição:</td>
                            <td>
                                <form:input path="descricao" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Data Saida:</td>
                            <td><form:input path="dataSaida" id="datepicker" /></td>
                        </tr>
                        <tr>
                            <td>Valor:</td>
                            <td><form:input path="valor" /></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Create" /></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </form:form>
        </div>
        <div id="tabela">
            <%@include file='lista.jsp'%>
        </div>
    </body>
</html>
