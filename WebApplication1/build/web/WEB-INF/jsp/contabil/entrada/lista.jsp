<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Size:${pessoas.size()}</h1>

<table border="1px" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th>idEntrada</th><th>descricao</th><th>Valor</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="en" items="${entradas}">
            <tr>
                <td>${en.idEntrada}</td>
                <td>${en.descricao}</td>
                <td><%--${en.dataEntrada}--%></td>
                <td>${en.valor}</td>
                <td>
                    <a href="/WebApplication1/entrada/edit/${en.idEntrada}.json">Edit</a><br/>
                    <a href="/WebApplication1/entrada/delete/${en.idEntrada}.json">Delete</a><br/>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>