<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>${smartphones.size()}</h1>
<table border="1px" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
        <th>Producer</th><th>Model</th><th>Price</th><th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="sPhone" items="${smartphones}">
            <tr>
                <td>${sPhone.producer}</td>
                <td>${sPhone.model}</td>
                <td>${sPhone.price}</td>
                <td>
                    <a href="/WebApplication1/smartphones/edit/${sPhone.price}.json">Edit</a><br/>
                    <a href="/WebApplication1/smartphones/delete/${sPhone.price}.json">Delete</a><br/>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>