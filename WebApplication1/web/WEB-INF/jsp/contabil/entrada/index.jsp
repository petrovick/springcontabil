<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <%--<script src="../../../js/jquery.min.js"></script>--%>
        <script type="text/javascript">
            $(document).ready(function() {
              $('#newEntradaForm').submit(function(event) {
                  /*var idPessoa = ; */
                  var idEntrada = 0;//parseInt($('#idEntrada').val(), 10) == null ? 0 : parseInt($('#idEntrada').val(), 10);
                  var descricao = $('#descricao').val();
                  var dataEntrada = "s";//$('#dataentrega').val();
                  var valor = parseFloat($('#valor').val());
                  var json = { "idEntrada" : idEntrada, "descricao" : descricao, "dataentrada": dataEntrada, "valor": valor};
                  
                $.ajax({
                        url: $("#newEntradaForm").attr( "action"),
                        data: JSON.stringify(json),
                        type: "POST",

                        beforeSend: function(xhr) {
                                xhr.setRequestHeader("Accept", "application/json");
                                xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(smartphone) {
                                var $container = $("#EntradaResponse");
                                $container.load("/WebApplication1/entrada/list");
                        }
                });
                event.preventDefault();
              });
            });   
        </script>
    </head>
    <body>
        <div id="cadastro">
            <form:form id="newEntradaForm" action="/WebApplication1/entrada/novo.json" commandName="entrada">
                <form:input path="idEntrada" hidden="true" /> 
                <table>
                    <tbody>
                        <tr>
                            <td>Descrição:</td>
                            <td>
                                <form:input path="descricao" />
                            </td>
                        </tr>
                        <tr>
                            <td>Valor:</td>
                            <td><form:input path="valor" /></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Create" /></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </form:form>
        </div>
        <div id="EntradaResponse">
            <%@include file='lista.jsp'%>
        </div>
    </body>
</html>
