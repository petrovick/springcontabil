<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="newPessoaForm" action="/WebApplication1/pessoa/novo.json" commandName="pessoa">
    <form:input path="idPessoa" hidden="true" /> 
    <table>
        <tbody>
            <tr>
                <td>First Name:</td>
                <td>
                    <form:input path="pnome" />
                </td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td><form:input path="unome" /></td>
            </tr>
            <tr>
                <td>CPF/CNPJ:</td>
                <td><form:input path="cpfcnpj" /></td>
            </tr>
            <tr>
                <td>Data Nascimento:</td>
                <td><form:input path="datanascimento" /></td>
            </tr>
            <tr>
                <td><input type="submit" value="Create" /></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</form:form>