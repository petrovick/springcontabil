<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Edit Smartphone</title>
<link href="../../resources/css/main.css" rel="stylesheet" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
   
    $(document).ready(function() {
      
      $('#editPessoaForm').submit(function(event) {
        
        var idPessoa = parseInt($('#idPessoa').val(), 10);
        var pnome = $('#pnome').val();
        var unome = $('#unome').val();
        var cpfcnpj = $('#cpfcnpj').val();
        var datanascimento = $('#datanascimento').val();
        var json = { "idPessoa" : idPessoa, "pnome" : pnome, "unome": unome, "cpfcnpj": cpfcnpj, "datanascimento": datanascimento};

        $.ajax({
        	url: $("#editPessoaForm").attr( "action"),
        	data: JSON.stringify(json),
        	type: "PUT",
        	
        	beforeSend: function(xhr) {
        		xhr.setRequestHeader("Accept", "application/json");
        		xhr.setRequestHeader("Content-Type", "application/json");
        	},
        	success: function(smartphone) {
                    window.location.href = "/WebApplication1/pessoa/index";
        	}
                
        });
        
        event.preventDefault();
      });
       
    });   
  </script>

</head>
<body>
    <div id="container">
        <h1>Edit Pessoa</h1>
        <form:form id="editPessoaForm" action="/WebApplication1/pessoa/edit/${pessoa.idPessoa}.json" commandName="pessoa">
           <form:input path="idPessoa" hidden="true"/> 
            <table>
                <tbody>
                    <tr>
                        <td>First Name:</td>
                        <td>
                            <form:input path="pnome" />
                        </td>
                    </tr>
                    <tr>
                        <td>Last Name:</td>
                        <td><form:input path="unome" /></td>
                    </tr>
                    <tr>
                        <td>CPF/CNPJ:</td>
                        <td><form:input path="cpfcnpj" /></td>
                    </tr>
                    <tr>
                        <td>Data Nascimento:</td>
                        <td><form:input path="datanascimento" /></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Create" /></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </form:form>
        <a href="${pageContext.request.contextPath}/index.html">Home page</a>
    </div>
</body>
</html>