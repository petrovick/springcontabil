<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Size:${pessoas.size()}</h1>
<table border="1px" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
        <th>IdPessoa</th><th>First Name</th><th>Last Name</th><th>CPF/CNPJ</th><th>Data Nascimento</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="pe" items="${pessoas}">
            <tr>
                <td>${pe.idPessoa}</td>
                <td>${pe.pnome}</td>
                <td>${pe.unome}</td>
                <td>${pe.cpfcnpj}</td>
                <td>${pe.datanascimento}</td>
                <td>
                    <a href="/WebApplication1/pessoa/edit/${pe.idPessoa}.json">Edit</a><br/>
                    <a href="/WebApplication1/pessoa/delete/${pe.idPessoa}.json">Delete</a><br/>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>