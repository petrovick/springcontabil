<%-- 
    Document   : index
    Created on : Oct 19, 2014, 1:39:42 AM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="../../../js/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
              $('#newPessoaForm').submit(function(event) {
                  /*var idPessoa = ; */
                  var idPessoa = parseInt($('#idPessoa').val(), 10);
                  var pnome = $('#pnome').val();
                  var unome = $('#unome').val();
                  var cpfcnpj = $('#cpfcnpj').val();
                  var datanascimento = $('#datanascimento').val();
                  var json = { "idPessoa" : idPessoa, "pnome" : pnome, "unome": unome, "cpfcnpj": cpfcnpj, "datanascimento": datanascimento};
                  
                $.ajax({
                        url: $("#newPessoaForm").attr( "action"),
                        data: JSON.stringify(json),
                        type: "POST",

                        beforeSend: function(xhr) {
                                xhr.setRequestHeader("Accept", "application/json");
                                xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(smartphone) {
                                var $container = $("#tabela");
                                $container.load("/WebApplication1/pessoa/list");
                        }
                });
                event.preventDefault();
              });
            });   
        </script>
    </head>
    <body>
        <h1>Hello World!</h1>
        <div id="cadastro">
            <%--
            <%@include file='cadastro.jsp'%>
            --%>
            <form:form id="newPessoaForm" action="/WebApplication1/pessoa/novo.json" commandName="pessoa">
                <form:input path="idPessoa" hidden="true" value="0" /> 
                <table>
                    <tbody>
                        <tr>
                            <td>First Name:</td>
                            <td>
                                <form:input path="pnome" />
                            </td>
                        </tr>
                        <tr>
                            <td>Last Name:</td>
                            <td><form:input path="unome" /></td>
                        </tr>
                        <tr>
                            <td>CPF/CNPJ:</td>
                            <td><form:input path="cpfcnpj" /></td>
                        </tr>
                        <tr>
                            <td>Data Nascimento:</td>
                            <td><form:input path="datanascimento" /></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Create" /></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </form:form>
        </div>
        <div id="tabela">
            <%--
            <%@include file='lista.jsp'%>
            --%>
            <table border="1px" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                    <th>IdPessoa</th><th>First Name</th><th>Last Name</th><th>CPF/CNPJ</th><th>Data Nascimento</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="pe" items="${pessoas}">
                        <tr>
                            <td>${pe.idPessoa}</td>
                            <td>${pe.pnome}</td>
                            <td>${pe.unome}</td>
                            <td>${pe.cpfcnpj}</td>
                            <td>${pe.datanascimento}</td>
                            <td>
                                <a href="/WebApplication1/pessoa/edit/${pe.idPessoa}.json">Edit</a><br/>
                                <a href="/WebApplication1/pessoa/delete/${pe.idPessoa}.json">Delete</a><br/>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
