$(document).ready(function() {
    $('#saveContact').submit(function (e) {
        $.post('/WebApplication1/contact/saveScript', $(this).serialize(), function (contact) {
            $('#contactTableResponse').last().append(
                '<tr>' +
                    '<td>' + contact.id + '</td>' +
                    '<td>' + contact.firstname + '</td>' + 
                    '<td>' + contact.lastname + '</td>' + 
                    '<td>' + contact.email + '</td>' +
                    '<td>' + contact.phone + '</td>' +
                '</tr>');
        });
        clearInputs(); //Limpar formulário
        e.preventDefault();
    });
});

function clearInputs()
{
    $('input[id*="Input"]').each(function() //procura por todos os ID's que termina com Input
    {
       $(this).val(''); 
    });
}