
package web.controller;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import webcontabilidade.application.factory.ApplicationFactory;
import webcontabilidade.application.interfaces.IEntradaApplication;
import webcontabilidade.business.Entrada;

@Controller
@RequestMapping(value = {"/entrada"})
public class EntradaController
{
    private Entrada entrada;
    IEntradaApplication entradaApplication = ApplicationFactory.getInstance().getEntradaApplication();
    public EntradaController(){}
    
    @RequestMapping(value = {"/index"})
    public ModelAndView index()
    {
        ModelAndView mav = new ModelAndView("contabil/entrada/index");
        mav.addObject("entrada", new Entrada());
        mav.addObject("entradas", entradaApplication.todos());
        return mav;
    }
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView listPhones() {
        List<Entrada> lista = entradaApplication.todos();
        return new ModelAndView("contabil/entrada/lista", "entradas", lista);
    }
    @RequestMapping(value="/novo", method=RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Entrada> createSmartphone(@RequestBody Entrada entrada) {
        if(entrada.getIdEntrada() == null || entrada.getIdEntrada()  == 0)
            entradaApplication.inserir(entrada);
    else
        entradaApplication.alterar(entrada);
            return entradaApplication.todos();
    }
}
