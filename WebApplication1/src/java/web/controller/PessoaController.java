package web.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import web.model.DataSet;
import web.model.Pessoa;
import web.service.PessoaService;
import web.service.PessoaServiceImpl;

@Controller
@RequestMapping(value = {"/pessoa"})
public class PessoaController
{
    
    //@Autowired
    private PessoaService pessoaService;
    
    public PessoaController()
    {
        pessoaService = new PessoaServiceImpl();
    }
    
    @RequestMapping(value="/novo", method=RequestMethod.POST, 
                    produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Pessoa> createPessoa(@RequestBody Pessoa pessoa) {
            pessoaService.create(pessoa);
            return DataSet.getPessoas();
    }
    
    @RequestMapping(value="/index", method=RequestMethod.GET)
    public ModelAndView index()
    {
        ModelAndView mav = new ModelAndView("pessoa/index");
        mav.addObject("pessoa", new Pessoa());
        mav.addObject("pessoas", DataSet.getPessoas());
        return mav;
    }
    
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView listPhones() {
        List<Pessoa> lista = pessoaService.getAll();
        return new ModelAndView("/pessoa/lista", "pessoas", lista);
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.PUT, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Pessoa editSmartphone(@PathVariable Integer id, 
                    @RequestBody Pessoa smartphone) {
//		smartphone.setId(id);
            return pessoaService.update(smartphone);
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editSmartphonePage(@PathVariable int id) {
            ModelAndView mav = new ModelAndView("pessoa/edit");
            Pessoa smartphone = pessoaService.get(id);
            mav.addObject("pessoa", smartphone);
            return mav;
    }
}
