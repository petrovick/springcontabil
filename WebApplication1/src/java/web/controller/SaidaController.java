package web.controller;

import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import web.model.Smartphone;
import webcontabilidade.application.factory.ApplicationFactory;
import webcontabilidade.application.interfaces.ISaidaApplication;
import webcontabilidade.business.Saida;

@Controller
@RequestMapping(value = {"/saida"})
public class SaidaController
{
    ISaidaApplication saidaApplication = ApplicationFactory.getInstance().getSaidaApplication();
    public SaidaController(){}
    
    @RequestMapping(value = {"/index"})
    public ModelAndView index()
    {
        ModelAndView mav = new ModelAndView("contabil/saida/index");
        mav.addObject("saida", new Saida());
        mav.addObject("saidas", saidaApplication.todos());
        return mav;
    }
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView listSaida() {
        List<Saida> lista = saidaApplication.todos();
        return new ModelAndView("contabil/saida/lista", "saidas", lista);
    }
    @RequestMapping(value="/novo", method=RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void createSaida(@RequestBody Saida saida) {
        if(saida.getIdSaida()== null || saida.getIdSaida()== 0)
            saidaApplication.inserir(saida);
    else
        saidaApplication.alterar(saida);
    }
    
    
    @RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE, 
                    produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void deleteSmartphone(@PathVariable int id) {
            saidaApplication.excluir(id);
    }
}
