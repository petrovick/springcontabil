package web.controller;

import java.util.Iterator;
import org.springframework.context.annotation.ComponentScan;
//import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import web.application.ContactApplication;
import web.model.ContactBean;
import web.model.ContactFormBean;
import web.model.DataSet;

@Controller
@EnableWebMvc 
@RequestMapping(value = {"/contact"})   
public class ContactController
{
//    private static final Logger LOGGER = Logger.getLogger(ContactController.class);
    
    //Permite beasear esse método somente pelo mapeamento da classe
    private ContactApplication contactApplication;
    public ContactController()
    {
        this.contactApplication = new ContactApplication();
    }
    
    @RequestMapping(value = {"/list.html"}, method = RequestMethod.GET)
    public ModelAndView list()
    {
//        if(LOGGER.isInfoEnabled())
//            LOGGER.info("List all contacts.");
        ContactFormBean contactForm = new ContactFormBean();
        contactForm.setContacts(DataSet.getContacts());
        return new ModelAndView("/contact/list", "contactForm", contactForm.getContacts());
    }
    
    
    @RequestMapping(value = {"/index.html"}, method = RequestMethod.GET)
    public ModelAndView index()
    {
        ContactFormBean contactForm = new ContactFormBean();
        contactForm.setContacts(DataSet.getContacts());
        return new ModelAndView("/contact/index", "contactForm", contactForm.getContacts());
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(@RequestParam("id") int id)
    {
        Iterator<ContactBean> contacts = DataSet.getContacts().iterator();
        while(contacts.hasNext())
        {
            ContactBean contact = contacts.next();
            if(contact.getId() == id)
                contacts.remove();
        }
        return new ModelAndView("/contact/index", "contactForm", contactApplication.todos());
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam("id") int id)
    {
        Iterator<ContactBean> contacts = DataSet.getContacts().iterator();
        while(contacts.hasNext())
        {
            ContactBean contact = contacts.next();
            if(contact.getId() == id)
                return new ModelAndView("/contact/addEdit", "contact", contact);
        }
        return this.list();
    }
    
    @RequestMapping(value = {"savescript"}, method = RequestMethod.POST)
    @ResponseBody
    public ContactBean saveContact(ContactBean contact)
    {
        if(contact.getId() == null || contact.getId() == 0)
            DataSet.addContact(contact);
        else
            DataSet.update(contact);
        return contact;
    }
    
    @RequestMapping(value="/save", method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute("contact") ContactBean contact, @RequestParam("action") String action )
    {
        if((action != null) && (action.trim().toLowerCase().equals("cancel")))
            return new ModelAndView("/contact/index", "contactForm", contactApplication.todos());
//        if(LOGGER.isInfoEnabled())
//            LOGGER.info(String.format("Saving the contact. [%s]", contact), args));
        if(contact == null)
        {
//            LOGGER.error("Contact not received.");
            return this.list();
        }
        else if(contact.getId() == null)
            DataSet.addContact(contact);
        else
            DataSet.update(contact);
        return new ModelAndView("/contact/index", "contactForm", contactApplication.todos());
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add()
    {
//        if(LOGGER.isEnabled())
//            LOGGER.info("Creating a new contact.");
        return "/contact/addEdit";
    }
    
    @RequestMapping(value = "listEdit", method = RequestMethod.GET)
    public ModelAndView listEdit()
    {
//        if(LOGGER.isInfoEnabled())
//            LOGGER.info("all contacts");
        ContactFormBean contactForm = new ContactFormBean();
        
        contactForm.setContacts(DataSet.getContacts());
        return new ModelAndView("/contact/listEdit", "contactForm", contactForm);
        
    }
    
    @RequestMapping(value = "saveList", method = RequestMethod.POST)
    public ModelAndView saveList(@ModelAttribute("contactForm") ContactFormBean contactForm, @RequestParam("action") String action)
    {
        if((action != null) && (action.trim().toLowerCase().equals("cancel")))
            return this.list();
//        if(LOGGER.isInfoEnabled())
//            LOGGER.info("Saving all contacts");
        DataSet.setContacts(contactForm.getContacts());
        return this.list();
    }
    
}

