package web.service;

import java.util.List;

import web.model.Smartphone;

public interface SmartphoneService {
	
	public Smartphone create(Smartphone sp);
	public Smartphone get(Integer id);
	public List<Smartphone> getAll();
	public Smartphone update(Smartphone sp) throws NullPointerException;
	public Smartphone delete(Integer id) throws NullPointerException;

}
