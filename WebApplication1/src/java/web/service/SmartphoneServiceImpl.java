package web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.model.DataSet;

import web.model.Smartphone;

@Service
@Transactional
public class SmartphoneServiceImpl implements SmartphoneService {
	
	private DataSet smartphoneRepository;

	@Override
	public Smartphone create(Smartphone sp) {
            DataSet.addSmartphone(sp);
            return DataSet.getPhones().get(0);
	}

	@Override
	public Smartphone get(Integer id) {
		return DataSet.getPhones().get(id);
	}

	@Override
	public List<Smartphone> getAll() {
		return DataSet.getPhones();
	}

	@Override
	public Smartphone update(Smartphone sp) throws NullPointerException {
		Smartphone sPhoneToUpdate = get(1);
		if (sPhoneToUpdate == null)
			throw new NullPointerException(sp.getModel() + " nulo.");
//		sPhoneToUpdate.update(sp);
		return sPhoneToUpdate;
	}

	@Override
	public Smartphone delete(Integer id) throws NullPointerException {
		Smartphone sPhone = get(id);
		if (sPhone == null)
			throw new NullPointerException(id.toString());
//		smartphoneRepository.delete(id);
		return sPhone;
	}

}
