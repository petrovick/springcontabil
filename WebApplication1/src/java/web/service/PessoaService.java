package web.service;
import java.util.List;
import web.model.Pessoa;
public interface PessoaService
{
    public Pessoa create(Pessoa p);
    public Pessoa get(Integer id);
    public List<Pessoa> getAll();
    public Pessoa update(Pessoa sp) throws NullPointerException;
    public Pessoa delete(Integer id) throws NullPointerException;
}
