package web.service;

import java.util.List;
import web.model.DataSet;
import web.model.Pessoa;

public class PessoaServiceImpl implements PessoaService
{

    @Override
    public Pessoa create(Pessoa p) {
        DataSet.addPessoa(p);
        return p;
    }

    @Override
    public Pessoa get(Integer id)
    {
        return (Pessoa) (DataSet.getPessoas().stream().filter(x -> x.getIdPessoa() == id).toArray())[0];
    }

    @Override
    public List<Pessoa> getAll() {
        return DataSet.getPessoas();
    }

    @Override
    public Pessoa update(Pessoa sp) throws NullPointerException {
        for(Pessoa p : DataSet.getPessoas())
        {
            if(p.getIdPessoa() == sp.getIdPessoa())
                p = sp;
        }
        return sp;
    }

    @Override
    public Pessoa delete(Integer id) throws NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
