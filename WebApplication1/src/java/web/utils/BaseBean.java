package web.utils;

import java.io.Serializable;
//import org.apache.commons.lang3.builder.EqualsBuilder;
//import org.apache.commons.lang3.builder.ToStringBuilder;
//import org.apache.commons.lang3.builder.ToStringStyle;


public class BaseBean implements Serializable
{
    @Override
    public String toString()
    {
        return null;
        //return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
    @Override
    public boolean equals(Object object)
    {
        return false;
        //return EqualsBuilder.reflectionEquals(this, object);
    }
    
}
