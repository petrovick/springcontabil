package web.application;

import java.util.List;
import web.model.ContactBean;
import web.model.ContactFormBean;
import web.model.DataSet;

public class ContactApplication
{
    public List<ContactBean> todos()
    {
        ContactFormBean contactForm = new ContactFormBean();
        contactForm.setContacts(DataSet.getContacts());
        return contactForm.getContacts();
    }
}
