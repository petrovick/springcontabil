package web.model;

import java.util.Date;

public class Pessoa
{
    private Integer IdPessoa;
    private String pnome;
    private String unome;
    private String cpfcnpj;
    private String datanascimento;

    public Pessoa(Integer IdPessoa, String pnome, String unome, String cpfcnpj, String dataNascimento) {
        this.IdPessoa = IdPessoa;
        this.pnome = pnome;
        this.unome = unome;
        this.cpfcnpj = cpfcnpj;
        this.datanascimento = dataNascimento;
    }
    
    public Pessoa(){}

    public Integer getIdPessoa() {
        return IdPessoa;
    }

    public void setIdPessoa(Integer IdPessoa) {
        this.IdPessoa = IdPessoa;
    }

    public String getPnome() {
        return pnome;
    }

    public void setPnome(String pnome) {
        this.pnome = pnome;
    }

    public String getUnome() {
        return unome;
    }

    public void setUnome(String unome) {
        this.unome = unome;
    }

    public String getCpfcnpj() {
        return cpfcnpj;
    }

    public void setCpfcnpj(String cpfcnpj) {
        this.cpfcnpj = cpfcnpj;
    }

    public String getDatanascimento() {
        return datanascimento;
    }

    public void setDatanascimento(String datanascimento) {
        this.datanascimento = datanascimento;
    }
    
    
    
}
