package web.model;
//Simula Banco de dados

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataSet
{
    private static List<ContactBean> contacts = new ArrayList<ContactBean>();
    private static List<Smartphone> phones = new ArrayList<Smartphone>();
    private static List<Pessoa> pessoas = new ArrayList<Pessoa>();
    private static int id = 0;
    private static int idS = 0;
    private static int idp = 0;
    
    static
    {
        contacts.add(new ContactBean(id++, "Gabriel", "Pet", "pet@gmail.com", "123"));
        contacts.add(new ContactBean(id++, "Pet", "Olive", "pet1@gmail.com", "1234"));
        contacts.add(new ContactBean(id++, "Oliveira", "Petrovick", "pet2@gmail.com", "1235"));
        contacts.add(new ContactBean(id++, "Santos", "Gabriel", "pet3@gmail.com", "1236"));
        contacts.add(new ContactBean(id++, "Teste", "testando", "pet4@gmail.com", "1237"));
        phones.add(new Smartphone("HTC1", "EVO1", ++idS));
        phones.add(new Smartphone("HTC2", "EVO2", ++idS));
        phones.add(new Smartphone("HTC3", "EVO3", ++idS));
        phones.add(new Smartphone("HTC4", "EVO4", ++idS));
        phones.add(new Smartphone("HTC5", "EVO5", ++idS));
        pessoas.add(new Pessoa(idp++, "Gabriel", "Petrovick", "10180179683", "2014-08-15"));
        pessoas.add(new Pessoa(idp++, "Giovani", "Dos Santos", "10180179684", "2014-08-16"));
        pessoas.add(new Pessoa(idp++, "Guilherme", "Antony", "10180179685", "2014-08-17"));
        pessoas.add(new Pessoa(idp++, "Patrick", "Santos", "10180179686", "2014-08-18"));
    }

    public static List<ContactBean> getContacts() {
        return contacts;
    }

    public static void setContacts(List<ContactBean> contacts) {
        DataSet.contacts = contacts;
    }
    public static void addContact(ContactBean contact)
    {
        contact.setId(id++);
        contacts.add(contact);
    }
    
    public static void update(ContactBean contact)
    {
        for(int i = 0 ; i < contacts.size() ; i++)
        {
            if(contacts.get(i).getId() == contact.getId())
                contacts.set(i, contact);
        }
    }

    public static List<Smartphone> getPhones() {
        return phones;
    }

    public static void setPhones(List<Smartphone> phones) {
        DataSet.phones = phones;
    }
    
    public static void addSmartphone(Smartphone smartphone)
    {
        phones.add(smartphone);
    }

    public static List<Pessoa> getPessoas() {
        return pessoas;
    }

    public static void setPessoas(List<Pessoa> pessoas) {
        DataSet.pessoas = pessoas;
    }
    
    public static void addPessoa(Pessoa pessoa)
    {
        pessoa.setIdPessoa(idp);
        pessoas.add(pessoa);
    }
}
