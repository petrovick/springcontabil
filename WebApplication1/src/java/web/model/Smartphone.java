package web.model;
public class Smartphone {
    private String producer;
    private String model;
    private float price;

    public Smartphone(String producer, String model, float price) {
        this.producer = producer;
        this.model = model;
        this.price = price;
    }
    
    public Smartphone(){}
    
    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    
}
