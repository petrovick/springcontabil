package web.model;

import java.util.List;
import web.utils.BaseBean;
public class ContactFormBean extends BaseBean
{
    private List<ContactBean> contacts;

    public ContactFormBean() {
    }

    public List<ContactBean> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactBean> contacts) {
        this.contacts = contacts;
    }
    
}
